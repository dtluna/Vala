using Gtk;

class Gui : Object {
    const string UI = "ui.glade";
    Gtk.Builder builder;
    Gtk.Window window;

    public Gui () {
        builder = new Gtk.Builder();
        try {
            builder.add_from_file(UI);
        } catch (Error e) {
            stderr.puts("Cannot initialize GUI! No UI file!");
        }
        
        window = builder.get_object("window") as Gtk.Window;
        window.destroy.connect(Gtk.main_quit);
        builder.connect_signals(this);
    }
    
    public void show(){
        window.show();
    }

    public void print(Gtk.Widget source) {
        stdout.puts("You have pressed a button!\n");
    }
}


class GtkApp : Object {
    public static int main(string [] args) {
        Gtk.init (ref args);
        var gui = new Gui();
        gui.show();
        Gtk.main();
        return 0;
    }
}
